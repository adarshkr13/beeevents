---
title: 'Wedding '
subtitle: Get Up and Running with Bee Events
category:
  - About Bee Events
author: Barath Gowda
date: 2019-08-03T19:59:59.000Z
featureImage: /uploads/getting-started-hero.jpg
---

Bee Events is a Nuxt.js template for generating a beautifully robust static site with blog.
